const std = @import("std");
const rl = @import("raylib");
const cr = @import("help.zig");

pub fn main() !void {
    rl.initWindow(cr.screenWidth, cr.screenHeight, "TicTacToe");
    defer rl.closeWindow();

    rl.setTargetFPS(60);

    while (!rl.windowShouldClose()) {
        rl.beginDrawing();
        defer rl.endDrawing();

        rl.clearBackground(rl.Color.white);
    }
}
